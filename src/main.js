import {createApp} from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMeta from 'vue-meta'

import ElementPlus from 'element-plus'
import locale from 'element-plus/lib/locale/lang/en'
import 'element-plus/lib/theme-chalk/index.css';
import '/src/assets/styles/style.css'
import utils from '/src/assets/js/utils.js'

import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faCameraRetro,
    faCog,
    faHeart,
    faHome,
    faLaptop,
    faLifeRing,
    faMobile,
    faRing,
    faShoppingBasket, faStar,
    faTshirt,
    faUserSecret
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faUserSecret,
    faMobile,
    faLaptop,
    faHome,
    faCameraRetro,
    faRing,
    faTshirt,
    faLifeRing,
    faShoppingBasket,
    faHeart,
    faCog,
    faStar)



createApp(App)
    .use(store)
    .use(router)
    .use(VueAxios, axios, utils, VueMeta)
    .use(ElementPlus, {locale})
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app');
